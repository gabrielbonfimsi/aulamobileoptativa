package br.com.doctum.optativa.aulamobile.telas;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import br.com.doctum.optativa.aulamobile.Produto;
import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.dao.ProdutoDAO;

public class CadProdActivity extends AppCompatActivity {

    TextInputEditText descricao,valor;
    int id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_prod);

        descricao = findViewById(R.id.descricao);
        valor = findViewById(R.id.valor);


        Bundle b = getIntent().getExtras();
        id = 0;


        if(b != null){
            Produto produto = (Produto) b.getSerializable("produto");
            descricao.setText(produto.getDescricao());
            valor.setText(produto.getValor());
            id = produto.getId();
        }
    }

    public void salvar(View view){
        String desc = descricao.getText().toString();
        String val = valor.getText().toString();

        ProdutoDAO pd = new ProdutoDAO(getApplicationContext());
        Produto p = new Produto();
        p.setDescricao(desc);
        p.setValor(val);
        if(id!=0){
            p.setId(id);
        }

        long id = pd.save(p);

        if(id != 0){
            startActivity(new Intent(getApplicationContext(),ListarActivity.class));
            pd.msg("Produto salvo com sucesso!");
            finish();
        }else {
            pd.msg("Erro ao salvar produto");
        }
    }

}
