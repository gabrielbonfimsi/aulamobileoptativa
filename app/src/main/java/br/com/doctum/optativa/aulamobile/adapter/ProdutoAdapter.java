package br.com.doctum.optativa.aulamobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.doctum.optativa.aulamobile.Produto;
import br.com.doctum.optativa.aulamobile.R;

public class ProdutoAdapter extends ArrayAdapter<Produto> {


    private List<Produto> items;

    public ProdutoAdapter(Context context, int textViewResourceId, List<Produto> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context ctx = getContext();
            LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista_produto, null);
        }
        Produto p = items.get(position);
        if (p != null) {
            ((TextView) v.findViewById(R.id.descricao)).setText("Descrição: " + p.getDescricao());
            ((TextView) v.findViewById(R.id.valor)).setText("Valor: " + p.getValor());
        }
        return v;
    }

}
