package br.com.doctum.optativa.aulamobile.telas;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import br.com.doctum.optativa.aulamobile.Produto;
import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.dao.UsuarioDAO;
import br.com.doctum.optativa.aulamobile.modelo.Usuario;

public class CadastrarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);

        Button button = (Button) findViewById(R.id.botaoCad);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText editText = (EditText) findViewById(R.id.newUser);
                String nomeUser = editText.getText().toString();

                Usuario usuario = new Usuario();
                usuario.setNome(nomeUser);

                UsuarioDAO usuarioDAO = new UsuarioDAO(getContext());
                usuarioDAO.save(usuario);

            }
        });
    }

    public Context getContext() {
        return this;
    }

}
