package br.com.doctum.optativa.aulamobile.telas;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import br.com.doctum.optativa.aulamobile.Produto;
import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.dao.ProdutoDAO;
import br.com.doctum.optativa.aulamobile.dao.UsuarioDAO;
import br.com.doctum.optativa.aulamobile.modelo.Usuario;

public class CadUserActivity extends AppCompatActivity {

    TextInputEditText email,nome,telefone,senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cad_user);


        email = findViewById(R.id.email);
        senha = findViewById(R.id.senha);
        nome = findViewById(R.id.nome);
        telefone = findViewById(R.id.telefone);
    }

    public void salvar(View view){
        String e = email.getText().toString();
        String n = nome.getText().toString();
        String t = telefone.getText().toString();
        String s = senha.getText().toString();

        UsuarioDAO u = new UsuarioDAO(getApplicationContext());
        Usuario user = new Usuario();
        user.setTelefone(t);
        user.setEmail(e);
        user.setNome(n);
        user.setSenha(s);

        long id = u.save(user);

        if(id != 0){
            u.msg("Por favor, digite sua credenciais!");
            startActivity(new Intent(getApplicationContext(),LoginActivity.class));
        }else {
            u.msg("Erro ao salvar usuário");
        }
    }

}
