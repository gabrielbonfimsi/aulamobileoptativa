package br.com.doctum.optativa.aulamobile.telas;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import br.com.doctum.optativa.aulamobile.Configuracoes;
import br.com.doctum.optativa.aulamobile.LifeCycle;
import br.com.doctum.optativa.aulamobile.PreferenciasUser;
import br.com.doctum.optativa.aulamobile.R;

public class MainActivity extends LifeCycle {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean isClicked = PreferenciasUser.isCheckNotification(this);
        Log.i("click", Boolean.toString(isClicked));

    }



    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId()) {

            case R.id.action_settings:
                Intent intent = new Intent(getApplicationContext(), Configuracoes.class);
                startActivity(intent);
                return true;

            case R.id.logout:
                Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent2);
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void selecionarOpcao(View v) {
        TextView textView = (TextView) v;

        switch(textView.getId()) {

            case R.id.iconCad:
                Log.i("click", "batman");
                Intent intent = new Intent(getApplicationContext(), ListarActivity.class);
                startActivity(intent);
                break;

            case R.id.iconEditar:
                Log.i("click", "ironman");
                Intent intent1 = new Intent(getApplicationContext(), CadProdActivity.class);
                startActivity(intent1);
                break;

            case R.id.iconListar:
                Log.i("click", "darthvader");
                Intent intentList = new Intent(getApplicationContext(), ListaServidorActivity.class);
                startActivity(intentList);
                break;
        }
    }

}
