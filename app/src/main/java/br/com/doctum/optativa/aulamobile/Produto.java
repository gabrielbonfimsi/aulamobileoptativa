package br.com.doctum.optativa.aulamobile;

import java.io.Serializable;

public class Produto implements Serializable {

    private String descricao;
    private String valor;
    private int id;

    public Produto(){}

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
