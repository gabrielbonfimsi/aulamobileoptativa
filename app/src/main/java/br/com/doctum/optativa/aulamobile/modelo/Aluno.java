package br.com.doctum.optativa.aulamobile.modelo;

import java.io.Serializable;

public class Aluno implements Serializable {

    private String alunos_email,alunos_nome;

    public String getAlunos_email() {
        return alunos_email;
    }

    public void setAlunos_email(String alunos_email) {
        this.alunos_email = alunos_email;
    }

    public String getAlunos_nome() {
        return alunos_nome;
    }

    public void setAlunos_nome(String alunos_nome) {
        this.alunos_nome = alunos_nome;
    }
}
