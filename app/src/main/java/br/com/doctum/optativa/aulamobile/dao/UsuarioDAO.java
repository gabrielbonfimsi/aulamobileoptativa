package br.com.doctum.optativa.aulamobile.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.doctum.optativa.aulamobile.modelo.Usuario;

public class UsuarioDAO extends Conexao{
    //nome do banco
    private SQLiteDatabase db;
    private static final String TAG = "sql";


    public UsuarioDAO(Context context){
        //context, nome do banco, factory, versao
        super(context);
        db = getConection();
    }

    //Insere um novo usuario, ou atualiza se ja existe
    public long save(Usuario usuario){
        long id = usuario.getId();

        try{
            ContentValues values = new ContentValues();
            values.put("usuario",usuario.getEmail());
            values.put("senha",usuario.getSenha());
            values.put("nome",usuario.getNome());
            values.put("telefone",usuario.getTelefone());
            if(id!=0){
                String _id = String.valueOf(usuario.getId());
                String[] whereArgs = new String[]{_id};
                //update usuario set values= ... where _id=?
                return db.update("usuario",values,"_id=?",whereArgs);
            }
            else{
                //insert into usuario values (...)
                return db.insert("usuario","",values);
            }
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }

    }

    public Usuario logar(Usuario u) {

        try{
            Cursor c = db.query("usuario", null, "usuario=? and senha=?", new String[]{ u.getEmail(),u.getSenha() }, null, null, null,null);
            Usuario user = null;
            if(c.moveToFirst()){
                do{
                    user = new Usuario();
                    //recupera os atributos de usuario
                    user.setId(c.getInt(c.getColumnIndex("_id")));
                    user.setEmail(c.getString(c.getColumnIndex("usuario")));
                }while(c.moveToNext());
            }
            return user;
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return null;
        }

    }


    //delete o usuario
    public int delete(Usuario usuario){

        try{
            //delete from usuario where _id=?
            int count = db.delete("usuario", "_id=?", new String[]{String.valueOf(usuario.getId())});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }

    }
    //deleta o usuario pelo nome
    public int deleteUsuarioByNome(String user){
        try{
            //delete from usuario where _id=?
            int count = db.delete("usuario", "usuario=?", new String[]{user});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }
    }
    //Consulta a lista com todos os usuarios
    public List<Usuario> findAll(){
        try{
            //select *from usuario
            Cursor c = db.query("usuario", null, null, null, null, null, null, null);
            return toList(c);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return null;
        }
    }

    //Lê o cursor e cria a lista de usuarios
    private List<Usuario> toList(Cursor c){
        List<Usuario> user = new ArrayList<Usuario>();

        if(c.moveToFirst()){
            do{
                Usuario u = new Usuario();
                user.add(u);
                //recupera os atributos de usuario
                u.setId(c.getInt(c.getColumnIndex("_id")));
                u.setEmail(c.getString(c.getColumnIndex("usuario")));
                u.setSenha(c.getString(c.getColumnIndex("senha")));
                u.setNome(c.getString(c.getColumnIndex("nome")));
                u.setTelefone(c.getString(c.getColumnIndex("telefone")));
            }while(c.moveToNext());
        }
        return user;
    }

    //Executa um SQL
    public void execSQL(String sql){
        try{
            db.execSQL(sql);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
        }
    }
    //Executa um SQL
    public void execSQL(String sql,Object[] args){
        try{
            db.execSQL(sql,args);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
        }
    }


}
