package br.com.doctum.optativa.aulamobile.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.modelo.Aluno;

public class AlunoAdapter extends ArrayAdapter<Aluno> {


    private List<Aluno> items;

    public AlunoAdapter(Context context, int textViewResourceId, List<Aluno> items) {
        super(context, textViewResourceId, items);
        this.items = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            Context ctx = getContext();
            LayoutInflater vi = (LayoutInflater)ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.item_lista_aluno, null);
        }
        Aluno aluno = items.get(position);
        if (aluno != null) {
            ((TextView) v.findViewById(R.id.nome)).setText("Nome: " + aluno.getAlunos_nome());
            ((TextView) v.findViewById(R.id.email)).setText("Email: " + aluno.getAlunos_email());
        }
        return v;
    }

}
