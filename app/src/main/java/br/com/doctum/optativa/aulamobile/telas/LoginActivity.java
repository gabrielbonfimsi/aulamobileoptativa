package br.com.doctum.optativa.aulamobile.telas;

import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import br.com.doctum.optativa.aulamobile.PreferenciasUser;
import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.dao.UsuarioDAO;
import br.com.doctum.optativa.aulamobile.modelo.Usuario;

public class LoginActivity extends AppCompatActivity {

    TextInputEditText email,nome,telefone,senha;
    CheckBox checkBox;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        email = findViewById(R.id.usuario);
        senha = findViewById(R.id.senha);

        checkBox = findViewById(R.id.checkbox);

        boolean isLogado = PreferenciasUser.getValuesBoolean(getApplicationContext(), "materConectado");

        if(isLogado){
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }

    }

    public void logar(View view) {

        UsuarioDAO user = new UsuarioDAO(getApplicationContext());
        Usuario u = new Usuario();

        u.setEmail(email.getText().toString());
        u.setSenha(senha.getText().toString());

        Usuario logado = user.logar(u);

        if(logado != null){
            if (checkBox.isChecked()) {
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", true);
            } else {
                PreferenciasUser.setValuesBoolean(getApplicationContext(), "materConectado", false);
            }
            startActivity(new Intent(getApplicationContext(),MainActivity.class));
        }else{
            user.msg("Erro ao realizar o login");
        }
    }

    public void redirect(View view) {
        startActivity(new Intent(getApplicationContext(),CadUserActivity.class));
    }
}
