package br.com.doctum.optativa.aulamobile.dao;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;

public class Conexao extends SQLiteOpenHelper {

    public static final String NOME_BANCO = "projeto_aula";
    private static final int VERSAO_BANCO = 1;
    private static final String TAG = "sql";
    private Context context;

    public Conexao(Context context){
        super(context,NOME_BANCO,null,VERSAO_BANCO);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db){
        Log.d(TAG, "Criando a tabela Usuario...");
        db.execSQL("create table if not exists usuario(_id integer primary key autoincrement," +
                "usuario text,senha text,nome text,telefone text);");
        db.execSQL("create table if not exists produto(_id integer primary key autoincrement," +
                "descricao text,valor text);");
        Log.d(TAG, "Tabela produto criada com sucesso");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db,int oldVersion,int newVersion){
        //Caso mude a versao do banco de dados, podemos executar um SQL aqui
    }

    SQLiteDatabase getConection(){
        return this.getWritableDatabase();
    }

    public void msg(String msg){
        Toast.makeText(this.context,msg,Toast.LENGTH_SHORT).show();
    }

}
