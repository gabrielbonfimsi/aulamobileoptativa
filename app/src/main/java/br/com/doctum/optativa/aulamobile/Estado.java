package br.com.doctum.optativa.aulamobile;

public class Estado {

    private String nome,sigla;

    public Estado() {
    }

    public String getNome() {
        return nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }
}
