package br.com.doctum.optativa.aulamobile.dao;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class CepCorreioTask extends AsyncTask<Void, Void, String> {

    private String cep;

    public CepCorreioTask(String cep) {
        this.cep = cep;
    }

    protected String doInBackground(Void... params) {
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        try {
            URL url = new URL("https://viacep.com.br/ws/"+this.cep+"/json/");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            connection.setRequestProperty("Content-type", "application/json");
            connection.setRequestProperty("Accept", "application/json");
            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.connect();

            Scanner scanner = new Scanner(url.openStream());
            StringBuilder resposta = new StringBuilder();
            while (scanner.hasNext()) {
                resposta.append(scanner.next());
            }
            return  resposta.toString();
        } catch (Exception e) {
            e.printStackTrace();
            if (urlConnection != null) {
                urlConnection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        return null;
    }

    protected void onPostExecute(String dados) {
        // Faça alguma coisa com os dados
    }
}
