package br.com.doctum.optativa.aulamobile.telas;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import br.com.doctum.optativa.aulamobile.CepCorreio;
import br.com.doctum.optativa.aulamobile.Estado;
import br.com.doctum.optativa.aulamobile.Produto;
import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.adapter.ProdutoAdapter;
import br.com.doctum.optativa.aulamobile.dao.GetAlunosTask;
import br.com.doctum.optativa.aulamobile.dao.ProdutoDAO;

public class ListarActivity extends AppCompatActivity {
    private static final String TAG = "services";
    private ListView lista;
    ProdutoDAO pd;
    ArrayList<Produto>  al;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);

        lista = findViewById(R.id.lista);

        pd = new ProdutoDAO(getApplicationContext());


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Produto p = al.get(position);
                Intent i = new Intent(getApplicationContext(),CadProdActivity.class);
                i.putExtra("produto",p);
                startActivity(i);
                finish();
            }
        });


        lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {


                AlertDialog.Builder dialog = new AlertDialog.Builder(ListarActivity.this);

                final Produto p = al.get(position);

                dialog.setTitle("Confirmar exclusão");
                dialog.setMessage("Deseja realmente excluir o produto ?");

                dialog.setPositiveButton("sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {


                        int del = pd.delete(p);
                        if(del!=0){
                            loadList();
                            Toast.makeText(getApplicationContext(), "Sucesso ao excluir produto!", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(getApplicationContext(), "Erro ao excluir produto!", Toast.LENGTH_LONG).show();
                        }

                    }

                });

                dialog.setNegativeButton("Não",null);

                dialog.create();
                dialog.show();
                return true;
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        loadList();
    }

    public void loadList(){
        al = (ArrayList<Produto>) pd.findAll();

        if(al != null) {
            ProdutoAdapter adapter = new ProdutoAdapter(ListarActivity.this, R.layout.item_lista_produto, al);
            lista.setAdapter(adapter);
        }
    }

    public static List<Estado> getEstados(Context context) {
        try {
            String json = readFile(context);
            List<Estado> estados = parserJSON(context, json);
            return estados;
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler os estados" + e.getMessage(), e);
            return null;
        }
    }

    public static List<Estado> getEstadosServer(Context context) {
        try {
            String json = new GetAlunosTask().execute().get();
            List<Estado> estados = parserJSON(context, json);
            return estados;
        } catch (Exception e) {
            Log.i(TAG, "Erro ao ler os estados" + e.getMessage(), e);
            return null;
        }
    }

    private static String loadJsonAssets(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("estados_brasileiros.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    private static String readFile(Context context) throws IOException {
        return loadJsonAssets(context);
    }


    private static List<Estado> parserJSON(Context context, String json) throws IOException {
        List<Estado> estados = new ArrayList<Estado>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("estados");
            JSONArray jsonEstados = obj.getJSONArray("estado");

            for (int i=0;i<jsonEstados.length();i++) {
                Estado e = new Estado();
                e.setNome(jsonEstados.getJSONObject(i).getString("nome"));
                e.setSigla(jsonEstados.getJSONObject(i).getString("sigla"));
                estados.add(e);
                Log.i(TAG, e.getSigla() + " - " + e.getNome());
            }
            Log.i(TAG, estados.size() + " encontrados");


        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return estados;
    }

    private static CepCorreio parserJSONCep(Context context, String json) throws IOException {
        CepCorreio cepCorreio = new CepCorreio();

        try {
            JSONObject root = new JSONObject(json);
            cepCorreio.setBairro(root.getString("bairro"));
            cepCorreio.setLogradouro(root.getString("logradouro"));
            cepCorreio.setLocalidade(root.getString("localidade"));

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return cepCorreio;
    }
}
