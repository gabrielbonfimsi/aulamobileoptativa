package br.com.doctum.optativa.aulamobile.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import br.com.doctum.optativa.aulamobile.Produto;

public class ProdutoDAO extends Conexao{

    private SQLiteDatabase db;
    private static final String TAG = "sql";


    public ProdutoDAO(Context context){
        //context, nome do banco, factory, versao
        super(context);
        db = getConection();
    }

    //Insere um novo produto, ou atualiza se ja existe
    public long save(Produto prod){
        long id = prod.getId();

        try{
            ContentValues values = new ContentValues();
            values.put("descricao",prod.getDescricao());
            values.put("valor",prod.getValor());
            if(id!=0){
                String _id = String.valueOf(prod.getId());
                String[] whereArgs = new String[]{_id};
                //update usuario set values= ... where _id=?
                return db.update("produto",values,"_id=?",whereArgs);
            }
            else{
                //insert into usuario values (...)
                return db.insert("produto","",values);
            }
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }

    }


    //delete o produto
    public int delete(Produto prod){

        try{
            //delete from usuario where _id=?
            int count = db.delete("produto", "_id=?", new String[]{String.valueOf(prod.getId())});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }

    }
    //deleta o produto pelo nome
    public int deleteUsuarioByNome(String prod){
        try{
            //delete from usuario where _id=?
            int count = db.delete("usuario", "usuario=?", new String[]{prod});
            Log.i(TAG, "Deletou [" + count + "] registro");
            return count;
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return 0;
        }
    }
    //Consulta a lista com todos os produtos
    public List<Produto> findAll(){
        try{
            //select *from usuario
            Cursor c = db.query("produto", null, null, null, null, null, null, null);
            return toList(c);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return null;
        }
    }
    //Consulta o produto pelo nome
    public List<Produto> findAllByTipo(String user){
        try{
            //select *from usuario where nome=?
            Cursor c = db.query("produto", null, "descricao='" + user + "'", null, null, null, null, null);
            return toList(c);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
            return null;
        }
    }
    //Lê o cursor e cria a lista de produtos
    private List<Produto> toList(Cursor c){
        List<Produto> prod = new ArrayList<Produto>();

        if(c.moveToFirst()){
            do{
                Produto p = new Produto();
                prod.add(p);
                //recupera os atributos de usuario
                p.setId(c.getInt(c.getColumnIndex("_id")));
                p.setDescricao(c.getString(c.getColumnIndex("descricao")));
                p.setValor(c.getString(c.getColumnIndex("valor")));
            }while(c.moveToNext());
        }
        return prod;
    }
    //Executa um SQL
    public void execSQL(String sql){
        try{
            db.execSQL(sql);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
        }
    }
    //Executa um SQL
    public void execSQL(String sql,Object[] args){
        try{
            db.execSQL(sql,args);
        }catch (Exception e){
            Log.i(TAG,e.getMessage());
        }
    }

}
