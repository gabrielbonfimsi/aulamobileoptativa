package br.com.doctum.optativa.aulamobile.telas;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import br.com.doctum.optativa.aulamobile.R;
import br.com.doctum.optativa.aulamobile.adapter.AlunoAdapter;
import br.com.doctum.optativa.aulamobile.dao.GetAlunosTask;
import br.com.doctum.optativa.aulamobile.modelo.Aluno;

public class ListaServidorActivity extends AppCompatActivity {

    ListView listaServidor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_servidor);

        listaServidor = findViewById(R.id.listaServidor);

        ArrayList<Aluno> al = (ArrayList<Aluno>) getAlunos(getApplicationContext());

        if(al != null) {
            AlunoAdapter adapter = new AlunoAdapter(ListaServidorActivity.this, R.layout.item_lista_aluno, al);
            listaServidor.setAdapter(adapter);
        }
    }

    private static List<Aluno> parserJSON(Context context, String json) throws IOException {
        List<Aluno> alunos = new ArrayList<>();

        try {
            JSONObject root = new JSONObject(json);
            JSONObject obj = root.getJSONObject("alunos");
            JSONArray jsonAlunos = obj.getJSONArray("aluno");


            for (int i=0;i<jsonAlunos.length();i++) {
                Aluno a = new Aluno();
                a.setAlunos_nome(jsonAlunos.getJSONObject(i).getString("alunos_nome"));
                a.setAlunos_email(jsonAlunos.getJSONObject(i).getString("alunos_email"));
                alunos.add(a);
            }

        } catch (JSONException e) {
            throw new IOException(e.getMessage(), e);
        }
        return alunos;
    }



    public static List<Aluno> getAlunos(Context context) {

        try {
            String json = new GetAlunosTask().execute().get();
            return parserJSON(context, json);
        } catch (Exception e) {
            Log.i("erro", e.getMessage());
            return null;
        }
    }


}
